# CPKNF

Flutter + Kotlin/Native

# Update Kotlin/Native logic library

Android - updated automatically, nothing to do

iOS:
1. run ./gradlew build in android/ directory
2. Copy /build/konan/bin/iphone-sim/Logic.framework to /ios/Runner
3. Use logic classes with prefix Logic (for example class BuildValues will be LogicBuildValues)
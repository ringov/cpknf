package com.ringov.cpknf

import android.os.Bundle
import android.util.Log
import com.ringov.cpknf.fluttermvp.FlutterDelegateHandlerGenerator
import com.ringov.cpknf.fluttermvp.FlutterMethodFactory
import com.ringov.cpknf.logic.SystemValues
import com.ringov.cpknf.logic.ToastDelegate
import com.ringov.cpknf.logic.ToastPresentation
import com.ringov.cpknf.mvp.MvpContext
import com.ringov.cpknf.mvp.MvpDelegateFactory
import com.ringov.cpknf.mvp.MvpViewStorage
import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterActivity() {

    private val system = SystemValues()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        system.systemName = "Android"
        Log.d("Android", "System Name: ${system.systemName}")
        GeneratedPluginRegistrant.registerWith(this)
        bind()
    }

    private fun bind() {
        val methodFactory = FlutterMethodFactory(flutterView)
        val viewStorage = MvpViewStorage()
        val delegateFactory = MvpDelegateFactory()

        val mvpContext = MvpContext(methodFactory, viewStorage, delegateFactory)

        mvpContext.addPresentation(ToastPresentation(methodFactory, FlutterDelegateHandlerGenerator(flutterView)))

        val presenter = mvpContext.getDelegate(ToastDelegate::class.java)

        Thread {
            for (i in 1..1000) {
                Thread.sleep(1000L)
                presenter.click()
            }

        }.start()
    }
}

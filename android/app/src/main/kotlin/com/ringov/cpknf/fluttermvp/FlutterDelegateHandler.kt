package com.ringov.cpknf.fluttermvp

import com.ringov.cpknf.mvp.DelegateHandler
import com.ringov.cpknf.mvp.MvpDelegate
import com.ringov.cpknf.mvp.MvpDelegateFactory
import com.ringov.cpknf.mvp.MvpViewStorage
import com.ringov.cpknf.uicore.MvpView
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class FlutterDelegateHandler<V : MvpView, D : MvpDelegate<V>>(messenger: BinaryMessenger,
                                                              name: String,
                                                              viewStorage: MvpViewStorage,
                                                              viewClass: Class<V>,
                                                              delegateFactory: MvpDelegateFactory,
                                                              delegateClass: Class<D>)
    : DelegateHandler<V, D>(viewStorage, viewClass, delegateFactory, delegateClass),
        MethodChannel.MethodCallHandler {

    private val channel = MethodChannel(messenger, name)

    init {
        channel.setMethodCallHandler(this)
    }

    override fun onMethodCall(method: MethodCall, result: MethodChannel.Result) {
        execute(method.method)
    }
}
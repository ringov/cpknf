package com.ringov.cpknf.fluttermvp

import com.ringov.cpknf.mvp.DelegateHandlerGenerator
import com.ringov.cpknf.mvp.MvpDelegate
import com.ringov.cpknf.uicore.MvpView
import io.flutter.plugin.common.BinaryMessenger

class FlutterDelegateHandlerGenerator<V : MvpView, D : MvpDelegate<V>>(messenger: BinaryMessenger)
    : DelegateHandlerGenerator<V, D>({ name, storage, viewClass, factory, delegateClass ->
    FlutterDelegateHandler(messenger, name, storage, viewClass, factory, delegateClass)
})
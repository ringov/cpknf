package com.ringov.cpknf.fluttermvp

import android.util.Log
import com.ringov.cpknf.uicore.ViewMethod
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.EventChannel

class FlutterMethod<T>(messenger: BinaryMessenger, name: String)
    : EventChannel.StreamHandler, ViewMethod<T> {

    private val channel = EventChannel(messenger, name)
    private var events: EventChannel.EventSink? = null

    init {
        Log.d("LGGR", "set stream handler to: " + channel.toString())
        channel.setStreamHandler(this)
    }

    override fun onListen(args: Any?, events: EventChannel.EventSink?) {
        Log.d("LGGR", "onListen")
        this.events = events
    }

    override fun onCancel(args: Any?) {
        Log.d("LGGR", "onCancel")
        this.events = null
    }

    override fun execute(arg: T) {
        Log.d("LGGR", "flutter execute, events: ${events?.toString()}, " + arg.toString())
        events?.success(arg)
    }
}
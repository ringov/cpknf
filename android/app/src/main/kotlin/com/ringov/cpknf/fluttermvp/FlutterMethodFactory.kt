package com.ringov.cpknf.fluttermvp

import com.ringov.cpknf.uicore.ViewMethodFactory
import io.flutter.plugin.common.BinaryMessenger

class FlutterMethodFactory(private val messenger: BinaryMessenger) : ViewMethodFactory {
    override fun <T> create(name: String) = FlutterMethod<T>(messenger, name)
}
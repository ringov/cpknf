package com.ringov.cpknf.logic

import com.ringov.cpknf.mvp.MvpDelegate

interface ToastDelegate : MvpDelegate<ToastView> {
    fun click()
}
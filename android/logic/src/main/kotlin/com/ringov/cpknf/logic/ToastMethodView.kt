package com.ringov.cpknf.logic

import com.ringov.cpknf.uicore.MethodView
import com.ringov.cpknf.uicore.ViewMethodFactory

class ToastMethodView(name: String, factory: ViewMethodFactory) : MethodView(name, factory), ToastView {

    override fun toast(message: String) {
        execute("toast", message)
    }
}
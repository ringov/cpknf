package com.ringov.cpknf.logic

import com.ringov.cpknf.mvp.DelegateHandlerGenerator
import com.ringov.cpknf.mvp.MethodPresentation
import com.ringov.cpknf.uicore.ViewMethodFactory

class ToastPresentation(viewMethodFactory: ViewMethodFactory,
                        delegateHandlerGenerator: DelegateHandlerGenerator<ToastView, ToastDelegate>)
    : MethodPresentation<ToastView, ToastDelegate>(viewMethodFactory, delegateHandlerGenerator) {
    override fun getName() = "toast"

    override fun getViewClass() = ToastView::class.java

    override fun getDelegateClass() = ToastDelegate::class.java

    override fun getView() = ToastMethodView(getName(), viewMethodFactory)

    override fun generateDelegate() = ToastPresenter()
}
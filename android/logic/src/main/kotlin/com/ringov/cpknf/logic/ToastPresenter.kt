package com.ringov.cpknf.logic

import com.ringov.cpknf.mvp.MvpPresenter

class ToastPresenter : MvpPresenter<ToastView>(), ToastDelegate {

    private var i = 0

    override fun click() {
        getView()?.toast("Hi $i")
        i++
    }
}
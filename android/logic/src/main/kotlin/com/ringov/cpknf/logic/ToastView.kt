package com.ringov.cpknf.logic

import com.ringov.cpknf.uicore.MvpView

interface ToastView: MvpView {
    fun toast(message: String)
}
package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView
import java.lang.reflect.Method

abstract class DelegateHandler<V : MvpView, D : MvpDelegate<V>>(private val viewStorage: MvpViewStorage,
                                                                private val viewClass: Class<V>,
                                                                private val delegateFactory: MvpDelegateFactory,
                                                                private val delegateClass: Class<D>) {

    companion object {
        private const val ATTACH_METHOD = "attach"
        private const val DETACH_METHOD = "detach"
    }

    private val methods = HashMap<String, Method>()
    private var delegate: D? = null

    init {
        collectMethods()
    }

    private fun collectMethods() {
        val methods = delegateClass.declaredMethods
        for (m in methods) {
            this.methods[m.name] = m
        }
    }

    fun execute(methodName: String) {
        if (methodName == ATTACH_METHOD) {
            getDelegate().attach(viewStorage.get(viewClass))
        } else {
            methods.getValue(methodName).invoke(delegate)
            if (methodName == DETACH_METHOD) {
                delegate = null
            }
        }

    }

    fun getDelegate(): D {
        if (delegate == null) {
            delegate = delegateFactory.create(delegateClass)
        }
        return delegate!!
    }
}
package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView

open class DelegateHandlerGenerator<V : MvpView, D : MvpDelegate<V>>(
        val callback: (String, MvpViewStorage, Class<V>, MvpDelegateFactory, Class<D>) -> DelegateHandler<V, D>)
package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView
import com.ringov.cpknf.uicore.ViewMethodFactory

abstract class MethodPresentation<V : MvpView, D : MvpDelegate<V>>(
        protected val viewMethodFactory: ViewMethodFactory,
        private val delegateHandlerGenerator: DelegateHandlerGenerator<V, D>) : MvpPresentation<V, D> {

    override fun generateDelegateHandler(viewStorage: MvpViewStorage,
                                         delegateFactory: MvpDelegateFactory): DelegateHandler<V, D> =
            delegateHandlerGenerator.callback(getName(), viewStorage, getViewClass(),
                    delegateFactory, getDelegateClass())
}
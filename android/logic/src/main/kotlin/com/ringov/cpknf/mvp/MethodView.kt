package com.ringov.cpknf.uicore

abstract class MethodView(private val name: String, private val factory: ViewMethodFactory) : MvpView {

    private val methodsList = ArrayList<String>()

    private val stringMethods = HashMap<String, ViewMethod<String>>()

    init {
        collectViewMethods()
        initializeViewMethods()
    }

    private fun collectViewMethods() {
        val interfaces = this.javaClass.interfaces
        for (i in interfaces) {
            if (MvpView::class.java.isAssignableFrom(i)) {
                methodsList.addAll(i.declaredMethods.map { it.name })
            }
        }
        if (methodsList.size < 1) {
            throw IllegalStateException("You do not implement any sub-interface of MvpView")
        }
    }

    private fun initializeViewMethods() {
        for (methodName in methodsList) {
            val method: ViewMethod<String> = factory.create("$name/$methodName")
            stringMethods[methodName] = method
        }
    }

    protected fun execute(name: String, value: String) {
        // do nothing if method with such name does not exists
        stringMethods[name]?.execute(value)
    }
}
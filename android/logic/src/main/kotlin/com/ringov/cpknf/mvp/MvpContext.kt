package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView
import com.ringov.cpknf.uicore.ViewMethodFactory

class MvpContext(private val viewMethodFactory: ViewMethodFactory,
                 private val viewStorage: MvpViewStorage,
                 private val delegateFactory: MvpDelegateFactory) {

    private val presentations = HashMap<Class<out MvpDelegate<out MvpView>>,
            DelegateHandler<out MvpView, out MvpDelegate<out MvpView>>>()

    @Deprecated(message = "Use addPresentation(MvpPresentation) instead")
    fun <V : MvpView, D : MvpDelegate<V>> addPresentation(name: String, viewClass: Class<V>,
                                                          viewGenerator: ViewGenerator<V>,
                                                          delegateClass: Class<D>,
                                                          delegateGenerator: DelegateGenerator<D>,
                                                          delegateHandlerGenerator: DelegateHandlerGenerator<V, D>) {
        viewStorage.addView(viewClass, viewGenerator.callback.invoke(name, viewMethodFactory))
        delegateFactory.addDelegate(delegateClass, MvpDelegateFactory.FactoryCallback { delegateGenerator.callback.invoke() })
        presentations[delegateClass] = createHandler(name, viewClass, delegateClass, delegateHandlerGenerator)
    }

    private fun <V : MvpView, D : MvpDelegate<V>> createHandler(name: String, viewClass: Class<V>,
                                                                delegateClass: Class<D>,
                                                                delegateHandlerGenerator: DelegateHandlerGenerator<V, D>):
            DelegateHandler<V, D> {
        return delegateHandlerGenerator.callback.invoke(name, viewStorage, viewClass, delegateFactory, delegateClass)
    }

    fun <V : MvpView, D : MvpDelegate<V>> addPresentation(presentation: MvpPresentation<V, D>) {
        val viewClass = presentation.getViewClass()
        viewStorage.addView(viewClass, presentation.getView())
        val delegateClass = presentation.getDelegateClass()
        delegateFactory.addDelegate(delegateClass, MvpDelegateFactory.FactoryCallback { presentation.generateDelegate() })
        presentations[delegateClass] = presentation.generateDelegateHandler(viewStorage, delegateFactory)
    }

    @Suppress("UNCHECKED_CAST")
    fun <V : MvpView, D : MvpDelegate<V>> getDelegate(delegateClass: Class<D>): D {
        return presentations.getValue(delegateClass).getDelegate() as D
    }

    class ViewGenerator<V>(val callback: (String, ViewMethodFactory) -> V)

    class DelegateGenerator<D>(val callback: () -> D)
}
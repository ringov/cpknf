package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView

interface MvpDelegate<V : MvpView> {
    fun attach(view: V)

    fun detach()
}
package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView

class MvpDelegateFactory {

    private val callbacks = HashMap<Class<out MvpDelegate<out MvpView>>,
            FactoryCallback<out MvpView, out MvpDelegate<out MvpView>>>()

    fun <V : MvpView, D : MvpDelegate<V>> addDelegate(delegateClass: Class<D>,
                                                      callback: FactoryCallback<V, D>) {
        callbacks[delegateClass] = callback
    }

    @Suppress("UNCHECKED_CAST")
    fun <V : MvpView, D : MvpDelegate<V>> create(delegateClass: Class<D>): D {
        return callbacks.getValue(delegateClass).callback.invoke() as D
    }

    data class FactoryCallback<V : MvpView, D : MvpDelegate<V>>(val callback: () -> D)
}
package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView

interface MvpPresentation<V : MvpView, D : MvpDelegate<V>> {

    fun getName(): String

    fun getViewClass(): Class<V>

    fun getDelegateClass(): Class<D>

    fun getView(): V

    fun generateDelegate(): D

    fun generateDelegateHandler(viewStorage: MvpViewStorage,
                                delegateFactory: MvpDelegateFactory): DelegateHandler<V, D>
}
package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView

abstract class MvpPresenter<V : MvpView> : MvpDelegate<V> {

    private var view: V? = null

    protected fun getView(): V? = view

    override fun attach(view: V) {
        this.view = view
    }

    override fun detach() {
        this.view = null
    }
}
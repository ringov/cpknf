package com.ringov.cpknf.mvp

import com.ringov.cpknf.uicore.MvpView

class MvpViewStorage {

    private val callbacks = HashMap<Class<out MvpView>, MvpView>()

    fun <V : MvpView> addView(viewClass: Class<V>, view: V) {
        callbacks[viewClass] = view
    }

    @Suppress("UNCHECKED_CAST")
    fun <V : MvpView> get(viewClass: Class<V>): V {
        return callbacks.getValue(viewClass) as V
    }
}
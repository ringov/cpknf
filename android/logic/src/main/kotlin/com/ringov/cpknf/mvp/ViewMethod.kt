package com.ringov.cpknf.uicore

interface ViewMethod<T> {
    fun execute(arg: T)
}
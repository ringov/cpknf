package com.ringov.cpknf.uicore

interface ViewMethodFactory {
    fun <T> create(name: String): ViewMethod<T>
}
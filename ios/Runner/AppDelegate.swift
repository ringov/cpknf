import UIKit
import Flutter
import Logic

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController;
    let channel = FlutterMethodChannel.init(name: "cpknf/system", binaryMessenger: controller);
    
    channel.setMethodCallHandler({
        (call: FlutterMethodCall, result: FlutterResult) -> Void in
        if ("getSystemName" == call.method) {
            result(self.getSystemName());
        } else {
            result(FlutterMethodNotImplemented);
        }
    });
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
   private func getSystemName() -> String {
        let system = LogicSystemValues()
        system.systemName = "iOS"
        return system.systemName
    }
}

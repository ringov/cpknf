import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

abstract class FlutterMvpView {}

abstract class FlutterMvpViewDelegate<V extends FlutterMvpView> {
  static const String _ATTACH_METHOD_NAME = "attach";
  static const String _DETACH_METHOD_NAME = "detach";

  @protected
  V view;

  Map<String, EventChannel> _stringCallbacks = Map();
  MethodChannel _methods;

  FlutterMvpViewDelegate(V view) {
    this.view = view;
    _methods = MethodChannel(name());
  }

  @protected
  String name();

  void observe();

  @protected
  Stream<String> observeString(String callbackName) {
    return _stringCallbacks
        .putIfAbsent(
            callbackName, () => EventChannel("${name()}/$callbackName"))
        .receiveBroadcastStream()
        .map(_convertToString);
  }

  String _convertToString(event) {
    return event.toString();
  }

  @protected
  void execute(String methodName) {
    _methods.invokeMethod(methodName);
  }

  void attach() {
    execute(_ATTACH_METHOD_NAME);
  }

  void detach() {
    execute(_DETACH_METHOD_NAME);
  }
}

abstract class FlutterMvpViewState<W extends StatefulWidget, V extends FlutterMvpView,
    D extends FlutterMvpViewDelegate<V>> extends State<W> implements FlutterMvpView {
  FlutterMvpViewDelegate<V> _delegate;

  FlutterMvpViewState() {
    _delegate = provideViewDelegate();
    _delegate.observe();
    _delegate.attach();
  }

  D provideViewDelegate();

  D getViewDelegate() {
    return _delegate;
  }
}

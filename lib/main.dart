import 'package:cpknf/mvp/toast.dart';
import 'package:cpknf/tutorials/one_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MaterialApp(title: 'App', home: ContentBody()));
}

class ContentBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ContentBodyState();
  }
}

class ContentBodyState extends ToastViewState<ContentBody> {
  System system = System();

  String systemName = "";

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.blueGrey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Hello, $systemName!',
              style: TextStyle(color: Colors.white, fontSize: 40.0),
              textAlign: TextAlign.center,
            ),
            RaisedButton(
                child: new Text("One Button"),
                onPressed: () => getViewDelegate().click(),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)))
          ],
        ),
      ),
    );
  }

  @override
  void toast(String message) {
    setState(() {
      print(message);
      systemName = message;
    });
  }
}

void _navigate(BuildContext context) {
  Navigator.of(context).push(MaterialPageRoute<Null>(
    builder: (BuildContext context) {
      return OneButton(100.0);
    },
  ));
}

class System {
  static System _instance;

  System.private(this._methodChannel);

  final EventChannel _methodChannel;

  Future<String> get systemName => _methodChannel
      .receiveBroadcastStream()
      .first
      .then<String>((dynamic result) => result);

  getSystemName() async {
    return "name";
  }

  factory System() {
    if (_instance == null) {
      EventChannel methodChannel = EventChannel('cpknf/system');
      print("LGGR: EventChannel created");
      _instance = System.private(methodChannel);
    }
    return _instance;
  }
}

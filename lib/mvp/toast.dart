import 'package:cpknf/fluttermvp/view.dart';
import 'package:flutter/widgets.dart';

abstract class ToastView extends FlutterMvpView {
  void toast(String message);
}

class ToastViewDelegate extends FlutterMvpViewDelegate<ToastView> {
  static const String _CLICK_METHOD_NAME = "click";

  ToastViewDelegate(ToastView view) : super(view);

  String name() {
    return "toast";
  }

  @override
  void observe() {
    observeString("toast").listen((String message) => view.toast(message));
  }

  void click() {
    execute(_CLICK_METHOD_NAME);
  }
}

abstract class ToastViewState<W extends StatefulWidget>
    extends FlutterMvpViewState<W, ToastView, ToastViewDelegate>
    implements ToastView {
  @override
  ToastViewDelegate provideViewDelegate() {
    return ToastViewDelegate(this);
  }
}

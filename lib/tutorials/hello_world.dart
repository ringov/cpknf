import 'package:flutter/widgets.dart';

class HelloWorld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'Hello, World!',
        textDirection: TextDirection.ltr,
      ),
    );
  }
}

import 'package:flutter/material.dart';

class OneButton extends StatefulWidget {
  final double _size;

  const OneButton(this._size);

  @override
  State<StatefulWidget> createState() {
    return OneButtonState(_size);
  }
}

class OneButtonState extends State<OneButton> {
  final double _size;

  OneButtonState(this._size);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 200.0),
              child: Center(
                child: Text(
                  'Hello!',
                  style: TextStyle(color: Colors.white, fontSize: 20.0),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Center(
              child: Container(
                width: _size,
                height: _size,
                child: RaisedButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(_size / 2)),
                  onPressed: () {},
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
